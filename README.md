# ISY-AI4VG

Image to Minecraft House Pipeline.

Upload an image of a real life house and the model creates a Minecraft schematic file to use in your world.

University project.

## Pipeline

- [X] Input the Image
- [X] Image segmentation masks out the house (from the background)
- [X] [Point-E](https://github.com/openai/point-e) creates a point cloud from the image
- [ ] The point cloud is made more dense 
- [X] Voxels (blocks and textures) will be created from the point cloud
- [X] A schematic file is generated

## Installation

Requires `python>=3.8`, `pytorch>=1.7` and `torchvision>=0.8`.

1. Install the models according to [this README](./models/README.md).
2. Download weights according to [this README](./weights/README.md).
3. Install NBT via ``pip install NBT``

## Run the model

The demo Notebook contains code to run the models.