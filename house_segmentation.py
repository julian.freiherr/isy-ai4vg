import os
import torch
import cv2
import numpy as np
from PIL import Image

from segment_anything import sam_model_registry, SamPredictor
from groundingdino.util.inference import Model
from typing import List
from segment_anything import SamPredictor

HOME = os.getcwd()
GROUNDING_DINO_CHECKPOINT_PATH = os.path.join(HOME, "weights", "groundingdino_swint_ogc.pth")
GROUNDING_DINO_CONFIG_PATH = os.path.join(HOME, "models/GroundingDINO/groundingdino/config/GroundingDINO_SwinT_OGC.py")
SAM_CHECKPOINT_PATH = os.path.join(HOME, "weights", "sam_vit_h_4b8939.pth")
SAM_ENCODER_VERSION = "vit_h"
CLASSES = ["house"]

DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def init_grounding_dino_model():
    grounding_dino_model = Model(model_config_path=GROUNDING_DINO_CONFIG_PATH,
                                 model_checkpoint_path=GROUNDING_DINO_CHECKPOINT_PATH)
    return grounding_dino_model


def init_sam_model():
    sam = sam_model_registry[SAM_ENCODER_VERSION](checkpoint=SAM_CHECKPOINT_PATH).to(device=DEVICE)
    sam_predictor = SamPredictor(sam)
    return sam_predictor


def enhance_class_name(class_names: List[str]) -> List[str]:
    return [
        f"all {class_name}s"
        for class_name
        in class_names
    ]


def segment(sam_predictor: SamPredictor, image: np.ndarray, xyxy: np.ndarray) -> np.ndarray:
    sam_predictor.set_image(image)
    result_masks = []
    for box in xyxy:
        masks, scores, logits = sam_predictor.predict(
            box=box,
            multimask_output=True
        )
        index = np.argmax(scores)
        result_masks.append(masks[index])
    return np.array(result_masks)


def segment_house(img, grounding_dino_model: Model, sam_predictor: SamPredictor, box_threshold=0.35, text_threshold=0.25):
    '''
    Segments house from a given image.

            Parameters:
                    img (str or PIL Image): Path to image or PIL Image to segment
                    grounding_dino_model: GroundingDINO model
                    sam_predictor: Predictor for Segment Anything Model
                    box_threshold (float): Box threshold
                    text_threshold (float): Text threshold

            Returns:
                    segmented (PIL image): Segmented image
    '''

    # load image
    if type(img) == str:
        img = Image.open(img)
    image = np.array(img)

    # detect objects
    detections = grounding_dino_model.predict_with_classes(
        image=image,
        classes=enhance_class_name(class_names=CLASSES),
        box_threshold=box_threshold,
        text_threshold=text_threshold
    )

    detections.mask = segment(
        sam_predictor=sam_predictor,
        image=image,
        xyxy=detections.xyxy
    )

    segmented = []
    for i in range(len(detections.mask)):
        img = image.copy()
        img[detections.mask[i] == False] = [255, 255, 255]
        segmented.append(img)
    bb = list(map(lambda x: int(x), detections.xyxy[0]))
    print(bb)
    return Image.fromarray(segmented[0][max(bb[1]-100, 0):bb[3]+100, max(bb[0]-100, 0):bb[2]+100, :]) # TODO: what if more than one house?


# def plot_list(segmented): # TODO?
#     if len(segmented) > 1:
#         f, ax = plt.subplots(len(segmented), 1)
#         for i in range(len(segmented)):
#             ax[i].imshow(segmented[i])
#     else:
#         plt.imshow(segmented[0])