from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

import house_segmentation
import img2pc
import pc2schem

def init_models(point_e_size='40M'):
    grounding_dino_model = house_segmentation.init_grounding_dino_model()
    sam_predictor = house_segmentation.init_sam_model()
    point_e_sampler = img2pc.init_model(name=point_e_size)
    suite = {
        'dino': grounding_dino_model,
        'sam': sam_predictor,
        'point-e': point_e_sampler
    }
    return suite

def img2schem(name, img, size, model_suite, verbose=True):
    '''
    Creates point cloud for given image.

            Parameters:
                    img (str or PIL Image): Path to image or PIL Image to create schematic of
                    suite (Models): Dino, Sam, and Point-E Models

            Returns:
                    schematic (Schematic File): Minecraft Schematic
    '''
    if type(img) == str:
        img = Image.open(img)
    
    if verbose:
        print('### Segmenting house...')
    segmented = house_segmentation.segment_house(img, model_suite['dino'], model_suite['sam'])
    if verbose:
        print('### Generating point cloud...')
    pc = img2pc.create_pointcloud(segmented, model_suite['point-e'])
    if verbose:
        print('### Extracting voxels...')
    voxels, palette = pc2schem.pc2voxels(pc, size)
    if verbose:
        print('### Creating schematic...')
    nbtfile = pc2schem.voxel_to_nbtfile(filename=name, block_data=voxels, palette=palette)
    if verbose:
        print('### DONE!')

    return nbtfile

def img2schem_plot(name, img, size, model_suite, verbose=True):
    '''
    Creates point cloud for given image.

            Parameters:
                    img (str or PIL Image): Path to image or PIL Image to create schematic of
                    suite (Models): Dino, Sam, and Point-E Models

            Returns:
                    schematic (Schematic File): Minecraft Schematic
    '''
    if type(img) == str:
        img = Image.open(img)
    
    if verbose:
        print('### Segmenting house...')
    segmented = house_segmentation.segment_house(img, model_suite['dino'], model_suite['sam'])
    if verbose:
        print('### Generating point cloud...')
    pc = img2pc.create_pointcloud(segmented, model_suite['point-e'])
    if verbose:
        print('### Extracting voxels...')
    voxels, palette = pc2schem.pc2voxels(pc, size)
    if verbose:
        print('### Creating schematic...')
    nbtfile = pc2schem.voxel_to_nbtfile(filename=name, block_data=voxels, palette=palette)
    if verbose:
        print('### DONE!')

    fig, axs = plt.subplots(1, 4, figsize=(15, 5))

    axs[0].imshow(img)
    axs[0].set_title('Original image')

    axs[1].imshow(segmented)
    axs[1].set_title('Segmented image')

    pc_fig = img2pc.plot(pc, grid_size=1)
    pc_fig.canvas.draw()
    image_flat = np.frombuffer(pc_fig.canvas.tostring_rgb(), dtype='uint8')
    axs[2].imshow(image_flat.reshape(*reversed(pc_fig.canvas.get_width_height()), 3))
    axs[2].set_title('Point cloud')

    schem_fig = pc2schem.plot_cube(voxels, angle=320)
    schem_fig.canvas.draw()
    image_flat = np.frombuffer(schem_fig.canvas.tostring_rgb(), dtype='uint8')
    axs[3].imshow(image_flat.reshape(*reversed(schem_fig.canvas.get_width_height()), 3))
    axs[3].set_title('Voxels')

    plt.tight_layout()

    return nbtfile, fig

    