from PIL import Image
import torch
from tqdm.auto import tqdm

from point_e.diffusion.configs import DIFFUSION_CONFIGS, diffusion_from_config
from point_e.diffusion.sampler import PointCloudSampler
from point_e.models.download import load_checkpoint
from point_e.models.configs import MODEL_CONFIGS, model_from_config
from point_e.util.plotting import plot_point_cloud
from point_e.util.point_cloud import PointCloud

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def init_model(name='40M', verbose=True):
    if verbose:
        print('[Point-E] creating base model...')
    base_name = f'base{name}' # use base40M or base300M or base1B for better results
    base_model = model_from_config(MODEL_CONFIGS[base_name], device)
    base_model.eval()
    base_diffusion = diffusion_from_config(DIFFUSION_CONFIGS[base_name])

    if verbose:
        print('[Point-E] creating upsample model...')
    upsampler_model = model_from_config(MODEL_CONFIGS['upsample'], device)
    upsampler_model.eval()
    upsampler_diffusion = diffusion_from_config(DIFFUSION_CONFIGS['upsample'])

    if verbose:
        print('[Point-E] downloading base checkpoint...')
    base_model.load_state_dict(load_checkpoint(base_name, device))

    if verbose:
        print('[Point-E] downloading upsampler checkpoint...')
    upsampler_model.load_state_dict(load_checkpoint('upsample', device))

    if verbose:
        print('[Point-E] creating sampler...')
    sampler = PointCloudSampler(
        device=device,
        models=[base_model, upsampler_model],
        diffusions=[base_diffusion, upsampler_diffusion],
        num_points=[1024, 4096 - 1024],
        aux_channels=['R', 'G', 'B'],
        guidance_scale=[3.0, 3.0],
    )

    if verbose:
        print('[Point-E] done')

    return sampler

def create_pointcloud(img, sampler):
    '''
    Creates point cloud for given image.

            Parameters:
                    img (str or PIL Image): Path to image or PIL Image to create pointcloud of
                    sampler (Point-E Sampler): Sampler for generating points

            Returns:
                    pointcloud (PointCloud): Class with coords and rgb values for point clouds
    '''
    if type(img) == str:
        img = Image.open(img)

    samples = None
    for x in tqdm(sampler.sample_batch_progressive(batch_size=1, model_kwargs=dict(images=[img]))):
        samples = x

    pc = sampler.output_to_point_clouds(samples)[0]
    pc = {
        'coords': pc.coords,
        'R': pc.channels['R'],
        'G': pc.channels['G'],
        'B': pc.channels['B']
    }
    return pc

def plot(pc, grid_size=3):
    pc = PointCloud(pc['coords'], {'R': pc['R'], 'G': pc['G'], 'B': pc['B']})
    return plot_point_cloud(pc, grid_size=grid_size, fixed_bounds=((-0.75, -0.75, -0.75),(0.75, 0.75, 0.75)))