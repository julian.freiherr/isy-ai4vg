## Install

1. Install [Point-E](https://github.com/openai/point-e)
   ```
   cd $AI4VG_ROOT/models
   git clone https://github.com/openai/point-e.git
   cd point-e
   pip install -e i
   ```
2. Install [GoundedSAM](https://github.com/IDEA-Research/Grounded-Segment-Anything)
   1. Install [GroundingDINO](https://github.com/IDEA-Research/GroundingDINO.git)
   ```
   cd $AI4VG_ROOT/models
   git clone https://github.com/IDEA-Research/GroundingDINO.git
   cd GroundingDINO
   git checkout -q 57535c5a79791cb76e36fdb64975271354f10251
   pip install -e .
   ```
   2. Install [Segment Anything](git+https://github.com/facebookresearch/segment-anything.git)
   ```
   pip install 'git+https://github.com/facebookresearch/segment-anything.git'
   ```
