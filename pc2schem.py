from nbt import nbt
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from matplotlib import ticker
import numpy as np


rgb_palette = {
    # "minecraft:oak_planks": {"R": 152, "G": 124, "B": 68},
    # "minecraft:spruce_planks": {"R": 103, "G": 73, "B": 39},
    # "minecraft:birch_planks": {"R": 211, "G": 198, "B": 133},
    # "minecraft:jungle_planks": {"R": 152, "G": 104, "B": 65},
    # "minecraft:acacia_planks": {"R": 189, "G": 100, "B": 55},
    # "minecraft:dark_oak_planks": {"R": 74, "G": 48, "B": 21},
    # "minecraft:stone": {"R": 112, "G": 112, "B": 112},
    # "minecraft:grass_block": {"R": 113, "G": 175, "B": 69},
    "minecraft:terracotta":            {"R": 147, "G":  87, "B":  61},
    "minecraft:white_terracotta":      {"R": 203, "G": 169, "B": 152},
    "minecraft:orange_terracotta":     {"R": 153, "G":  69, "B":  24},
    "minecraft:magenta_terracotta":    {"R": 139, "G":  75, "B":  96},
    "minecraft:light_blue_terracotta": {"R": 103, "G":  98, "B": 129},
    "minecraft:yellow_terracotta":     {"R": 177, "G": 121, "B":  20},
    "minecraft:lime_terracotta":       {"R": 88,  "G": 104, "B":  38},
    "minecraft:pink_terracotta":       {"R": 155, "G":  68, "B":  67},
    "minecraft:gray_terracotta":       {"R": 43,  "G":  27, "B":  20},
    "minecraft:light_gray_terracotta": {"R": 123, "G":  93, "B":  83},
    "minecraft:cyan_terracotta":       {"R": 73,  "G":  79, "B":  79},
    "minecraft:purple_terracotta":     {"R": 105, "G":  55, "B":  72},
    "minecraft:blue_terracotta":       {"R": 62,  "G":  43, "B":  78},
    "minecraft:brown_terracotta":      {"R": 64,  "G":  38, "B":  20},
    "minecraft:green_terracotta":      {"R": 62,  "G":  69, "B":  28},
    "minecraft:red_terracotta":        {"R": 132, "G":  49, "B":  33},
    "minecraft:black_terracotta":      {"R": 22,  "G":   6, "B":   2}
    }


# Mostly from https://terbium.io/2017/12/matplotlib-3d/
def normalize(arr):
    arr_min = np.min(arr)
    return (arr-arr_min)/(np.max(arr)-arr_min)


def explode(data):
    shape_arr = np.array(data.shape)
    size = shape_arr[:3]*2 - 1
    exploded = np.zeros(np.concatenate([size, shape_arr[3:]]), dtype=data.dtype)
    exploded[::2, ::2, ::2] = data
    return exploded


def expand_coordinates(indices):
    x, y, z = indices
    x[1::2, :, :] += 1
    y[:, 1::2, :] += 1
    z[:, :, 1::2] += 1
    return x, y, z


def plot_cube(cube, legend=None, angle=320):
    n_cube = normalize(cube)

    facecolors = cm.viridis(n_cube)
    facecolors[:,:,:,-1] = facecolors[:,:,:,-1] * 0.75
    facecolors = explode(facecolors)

    if legend:
        if 'minecraft:air' in legend:
            filled = cube != legend['minecraft:air'].value
        else:
            filled = np.ones(cube.shape)
    else:
        # Assume that air is 0
        filled = cube != 0

    filled = explode(filled)

    x, y, z = expand_coordinates(np.indices(np.array(filled.shape) + 1))

    fig = plt.figure(figsize=(30/2.54, 30/3.54))
    ax = fig.add_subplot(111, projection='3d')
    max_dimension = max(cube.shape) *2
    ax.view_init(30, angle)
    ax.set_xlim(right=max_dimension)
    ax.set_ylim(top=max_dimension)
    ax.set_zlim(top=max_dimension)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(2))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(2))
    ax.zaxis.set_major_locator(ticker.MultipleLocator(2))

    ax.voxels(z, y, x, filled, facecolors=facecolors, edgecolors='gray', shade=False)

    if legend:
        cmap = cm.viridis
        norm = plt.Normalize(vmin=cube.min(), vmax=cube.max())
        color_map = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        color_map.set_array([])
        cbar = plt.colorbar(color_map, ax=ax)
        cbar.set_label('Blocks')

        cbar.set_ticks([norm(legend[name].value*cube.max()) for name in legend.keys()])
        cbar.set_ticklabels(legend.keys())

    return fig


def plot_schem(nbtfile):
    width = nbtfile['Width'].value
    height = nbtfile['Height'].value
    length = nbtfile['Length'].value
    blocks = np.array(nbtfile["BlockData"]).reshape((height, length, width))
    return plot_cube(blocks, nbtfile["Palette"], angle=140)


def get_block_from_color(r, g, b):
    min_dist =  255 ** 2 * 3
    for block_type, rgb_dict in rgb_palette.items():
        dist = (r - rgb_dict["R"]) **2 + (g - rgb_dict["G"])**2 + (b - rgb_dict["B"])**2
        if dist < min_dist:
            min_dist = dist
            block = block_type
    return block


def pc2voxels_ver1(pc, size=12):
    point_coords = ((pc["coords"] + 0.5) * size).astype(int)
    point_coords[:,[1,2,0]] = point_coords[:, [0, 1, 2]]
    voxels = np.zeros((size, size, size), dtype="int64")

    schem_palette = {"minecraft:air": 0}
    palette_max = 0
    for i in range(len(point_coords)):
        if not voxels[tuple(point_coords[i])]:
          r, g, b = pc['R'][i]*255, pc['G'][i]*255, pc['B'][i] * 255
          block_type = get_block_from_color(r, g, b)
          if block_type not in schem_palette:
            palette_max += 1
            schem_palette[block_type] = palette_max
          voxels[tuple(point_coords[i])] = schem_palette[block_type]
    return voxels, schem_palette


def pc2voxels_ver2(pc, size=12):
    point_coords = ((pc["coords"] + 0.5) * (size - 1)).astype(int)
    point_coords[:, [1, 2, 0]] = point_coords[:, [0, 1, 2]]
    voxels = np.zeros((size, size, size, len(rgb_palette) + 1))
    voxels[:, :, :, 0] = np.ones((size, size, size)) * 0.5

    schem_palette = {"minecraft:air": 0}
    palette_max = 0
    for i in range(len(point_coords)):
        r, g, b = pc['R'][i] * 255, pc['G'][i] * 255, pc['B'][i] * 255
        block_type = get_block_from_color(r, g, b)
        if block_type not in schem_palette:
            palette_max += 1
            schem_palette[block_type] = palette_max
        voxels[tuple(point_coords[i]) + (schem_palette[block_type],)] += 1
    voxels = np.argmax(voxels, axis=3)
    return voxels, schem_palette


def pc2voxels(pc, size=12):
    return pc2voxels_ver2(pc, size=size)


def voxel_to_nbtfile(filename, block_data, offset=None, version=2, data_version=3465, width=None, height=None, length=None, metadata=None, palette=None, block_entities=None):
    nbtfile = nbt.NBTFile()
    nbtfile.name = "Schematic"
    nbtfile.filename = 'output/' + filename + '.schem'
    nbtfile.tags.append(nbt.TAG_Int(name = "Version", value=version))
    nbtfile.tags.append(nbt.TAG_Int(name = "DataVersion", value=data_version))

    if metadata is not None:
        nbtfile.tags.append(metadata)
    else:
        metadata = nbt.TAG_Compound(name="Metadata")
        metadata.tags.append(nbt.TAG_Int(name = "WEOffsetX", value=0))
        metadata.tags.append(nbt.TAG_Int(name = "WEOffsetY", value=0))
        metadata.tags.append(nbt.TAG_Int(name = "WEOffsetZ", value=0))
        nbtfile.tags.append(metadata)

    if not (height and length and width):
        height, length, width = np.array(block_data).shape

    nbtfile.tags.append(nbt.TAG_Short(name="Width", value = width))
    nbtfile.tags.append(nbt.TAG_Short(name="Height", value = height))
    nbtfile.tags.append(nbt.TAG_Short(name="Length", value = length))

    if offset is not None:
        offset_tag = nbt.TAG_Int_Array(name="Offset")
        offset_tag.value = offset
        nbtfile.tags.append(offset_tag)

    if palette is not None:
        schem_palette = nbt.TAG_Compound(name = "Palette")
        palette_max = 0
        for block, value in palette.items():
            schem_palette.tags.append(nbt.TAG_Int(name=block, value = value))
            if value+1 > palette_max:
                palette_max = value+1
        nbtfile.tags.append(nbt.TAG_Int(name = "PaletteMax", value = palette_max))
        nbtfile.tags.append(schem_palette)

    tag_byte_array = nbt.TAG_Byte_Array(name='BlockData')
    tag_byte_array.value = bytearray(np.array(block_data, dtype="uint8").flatten())
    nbtfile.tags.append(tag_byte_array)

    if block_entities is not None:
        nbtfile.tags.append(block_entities)

    return nbtfile
